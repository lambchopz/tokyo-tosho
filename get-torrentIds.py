from sys import exit
import requests

#issue with exporting exe since SSL certificates aren't exported.
#verify=False disables SSL
def main():
	url = input("Enter in your url to collect ids:\n")
	filename = input("\nEnter output filename so save ids to:\n")
	f = open(filename,'w')

	print('\nSending Request...')
	r = requests.post(url,verify=False)

	page = requests.get(url,verify=False)
	text = page.text

	idLoc = 0
	ids = []
	while True:
		idLoc = text.find('ID: ', idLoc+10)
		
		if idLoc == -1:
			break

		ids.append(text[idLoc+4:idLoc+10])


	for Id in ids:
		f.write(Id + " ")

	print("ids written to: " + filename)

if __name__ == "__main__":
	exit(main())
