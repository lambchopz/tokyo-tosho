from sys import exit
import requests

#issue with exporting exe since SSL certificates aren't exported.
#verify=False disables SSL
def main():
	apikey = input("Enter in your generated api-key:\n")
	torrentIds = input("\nEnter in torrent ids separated by spaces:\n")

	torrents = torrentIds.split()

	pl = {'torrent_id[]':torrents,'action':'delete','apikey':apikey}

	print('Sending Request...')
	r = requests.post('https://www.tokyotosho.info/delete_torrent.php',params=pl,verify=True)

	page = requests.get('https://www.tokyotosho.info/delete_torrent.php',verify=True)
	if page.status_code == requests.codes.ok:
		print('Request Successful, please wait for your request to be processed')

if __name__ == "__main__":
	exit(main())
